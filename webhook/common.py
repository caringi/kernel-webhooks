"""Common code that can be used by all webhooks."""
import argparse
import os
import re
from urllib import parse

from cki_lib.gitlab import get_instance
from cki_lib.messagequeue import Message


def get_arg_parser():
    """Intialize a commandline parser.

    Returns: argparse parser.
    """
    parser = argparse.ArgumentParser(
        description='Manual handling of merge requests')
    parser.add_argument('--merge-request',
                        help='Process given merge request URL only')
    parser.add_argument('--action', default='',
                        help='Action for the MR when using URL only')
    parser.add_argument('--oldrev', action='store_true',
                        help='Treat this as changed MR when using URL only')
    return parser


def parse_mr_url(url):
    """Parse the merge request URL used for manual handlers.

    Args:
        url: Full merge request URL.

    Returns:
        A tuple of (gitlab_instance, mr_object, project_path_with_namespace).
    """
    url_parts = parse.urlsplit(url)
    instance_url = parse.urlunparse(url_parts[:2] + ('',) * 4)
    gl_instance = get_instance(instance_url)
    match = re.match(r'/(.*)/merge_requests/(\d+)', url_parts.path)
    project_path = re.sub('/-$', '', match[1])
    gl_project = gl_instance.projects.get(project_path)
    gl_mergerequest = gl_project.mergerequests.get(int(match[2]))

    return gl_instance, gl_mergerequest, project_path


# pylint: disable=unused-argument
def process_message(routing_key, payload, webhooks):
    """Process a webhook message."""
    object_kind = payload.get('object_kind')
    if not object_kind:
        return False  # unit tests
    if object_kind not in webhooks:
        return False  # unit tests

    message = Message(payload)
    with message.gl_instance() as gl_instance:
        webhooks[object_kind](gl_instance, message)

    return True  # unit tests


def consume_queue_messages(webhook_prefix, queue, exchange, webhooks):
    """Begin processing the main loop by reading messages from the queue."""
    routing_key_name = f'{webhook_prefix}_ROUTING_KEYS'
    queue_name = f'{webhook_prefix}_QUEUE'
    queue.consume_messages(
        exchange,
        os.environ.get(routing_key_name).split(),
        lambda routing_key, payload: process_message(
            routing_key, payload, webhooks
        ),
        os.environ.get(queue_name)
    )
